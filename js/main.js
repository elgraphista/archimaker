window.addEventListener('load', () => {
  const loader = document.querySelector('.loader');
  loader.className += ' hidden';
})



function shareUrl() {
  var copyText = document.getElementById("urlArchimaker");
  var btnUrl = document.getElementById("btnUrl");

  copyText.select();
  copyText.setSelectionRange(0, 99999); /*For mobile devices*/

  document.execCommand("copy");

  btnUrl.textContent = '¡Listo, comparte Archimaker.cl!'
}


if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0) 
{
   document.getElementsByTagName("BODY")[0].className += " safari";
}



/* Fab Button */

// toggleFab();

//Fab click
$('#prime').click(function() {
  toggleFab();
});
$('#btnOpenControl').click(function() {
  toggleFab();
});
$('#btnOpenControl1').click(function() {
  toggleFab();
});

//Toggle chat and links
function toggleFab() {
  $('.prime').toggleClass('is-active');
  $('#prime').toggleClass('is-float');
  $('.fab-archimaker').toggleClass('is-visible');
  
}

// Ripple effect
var target, ink, d, x, y;
$(".fab-archimaker").click(function(e) {
  target = $(this);
  //create .ink element if it doesn't exist
  if (target.find(".ink").length == 0)
    target.prepend("<span class='ink'></span>");

  ink = target.find(".ink");
  //incase of quick double clicks stop the previous animation
  ink.removeClass("animate");

  //set size of .ink
  if (!ink.height() && !ink.width()) {
    //use parent's width or height whichever is larger for the diameter to make a circle which can cover the entire element.
    d = Math.max(target.outerWidth(), target.outerHeight());
    ink.css({
      height: d,
      width: d
    });
  }

  //get click coordinates
  //logic = click coordinates relative to page - parent's position relative to page - half of self height/width to make it controllable from the center;
  x = e.pageX - target.offset().left - ink.width() / 2;
  y = e.pageY - target.offset().top - ink.height() / 2;

  //set the position and add class .animate
  ink.css({
    top: y + 'px',
    left: x + 'px'
  }).addClass("animate");
});


// Panels

const panelControl = document.getElementById('panelControl');
const asideProyecto = document.getElementById('asideProyecto');
const btnOpenControl = document.getElementById('btnOpenControl');
const btnOpenControl1 = document.getElementById('btnOpenControl1');
const btnOpenAside = document.getElementById('btnOpenAside');
const cardsContainer = document.getElementById('cards-container');
const btnCloseControl = document.getElementById('close-control');


/* btnOpenControl.addEventListener('click', () => {
  panelControl.classList.toggle('open');
  console.log('open')
});
 */
/* btnOpenControl1.addEventListener('click', () => {
  panelControl.classList.toggle('open');
}); */

/* btnCloseControl.addEventListener('click', () => {
  panelControl.classList.remove('open');
  console.log('open')
}); */

/* btnOpenAside.addEventListener('click', () => {
  asideProyecto.classList.toggle('open');
  console.log('open')
}); */



// Dom cards
const cardsEl = [];

// Cards Data

const cardsData = [
  {
    nombre: 'Av. Vitacura 2020',
    permiso: 'Permiso edificacion 1',
    srcImg: 'https://images.unsplash.com/photo-1580587771525-78b9dba3b914?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1934&q=80'
  },
  {
    nombre: 'Francisco Bilbao 325',
    permiso: 'Permiso edificacion 2',
    srcImg: 'https://images.unsplash.com/photo-1576941089067-2de3c901e126?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2343&q=80'
  },
  {
    nombre: 'Av. Las Condes 2461',
    permiso: 'Permiso edificacion 3',
    srcImg: 'https://images.unsplash.com/photo-1582268611958-ebfd161ef9cf?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2100&q=80'
  },
  {
    nombre: 'Av. Vitacura 2020',
    permiso: 'Permiso edificacion 1',
    srcImg: 'https://images.unsplash.com/photo-1564013799919-ab600027ffc6?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2100&q=80'
  },
  {
    nombre: 'Francisco Bilbao 325',
    permiso: 'Permiso edificacion 2',
    srcImg: 'https://images.unsplash.com/photo-1584738766473-61c083514bf4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2100&q=80'
  },
  {
    nombre: 'Av. Las Condes 2461',
    permiso: 'Permiso edificacion 3',
    srcImg: 'https://images.unsplash.com/photo-1591474200742-8e512e6f98f8?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1934&q=80'
  },

];

// Create all Cards
function createCards() {
  cardsData.forEach((data, index) => createCard(data, index));
}


// Create a single card in DOM
function createCard(data, index) {
  const card = document.createElement('div');
  card.classList.add('col-12', 'col-sm-6', 'col-lg-3', 'col-xl-2');

  if (index === 0) {
    card.classList.add('active');
  }

  card.innerHTML = `
  <div class="card mb-3">
    <div class="cont-img-top">
        <img class="card-img-top" src="${ data.srcImg}" alt="${ data.nombre}">
    </div>
    <div class="card-body">
      <h1 class="card-title">
          ${data.nombre}
        <small>${data.permiso}</small>
      </h1>
      <div class="acciones-proyecto">
            <a href="#" class="btn btn-outline-danger">
              <i class="fal fa-trash"></i>
            </a>
              <a href="#" class="btn btn-outline-dark">
                <i class="fal fa-pen"></i>
              </a>
              <a href="#" class="btn btn-outline-dark">
                <i class="fal fa-download"></i> 
              </a>
              <a href="#" class="btn btn-outline-dark">
                <i class="fal fa-check"></i>
              </a>
        </div>
      </div>
    </div>
  `;

  // card.addEventListener('click', () => card.classList.toggle('show-answer'));

  // Add to DOM cards
cardsEl.push(card);

cardsContainer.appendChild(card);

}

createCards();